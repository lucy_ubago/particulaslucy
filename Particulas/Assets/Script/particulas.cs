using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class particulas : MonoBehaviour
{
  
    public ParticleSystem Humo_Colorido;
    public ParticleSystem Bichitos;
    public ParticleSystem Snow;

    [Header("Purple Colors")]
    public Color Humo_ColoridoPurpleColor;
    public Color BichitosPurpleColor1;
    public Color SnowPurpleColor2;

    [Header("Blue Colors")]
    public Color Humo_ColoridoBlueColor;
    public Color BichitosBLueColor1;
    public Color SnowBLueColor2;

    [Header("Pink Colors")]
    public Color Humo_ColoridoPinkColor;
    public Color BichitosPinkColor1;
    public Color SnowPinkColor2;

    private bool playing;
    // Start is called before the first frame update
    void Start()
    {
        playing = true;

    }

    // Update is called once per frame
    void Update()
    {
        playing = !playing;
      
        var emission = Humo_Colorido.emission;
        emission.enabled = playing;

        emission = Bichitos.emission;
        emission.enabled = playing;

        emission = Snow.emission;
        emission.enabled = playing;
    }
    public void SetColorPurple()
    {
        var main = Humo_Colorido.main;
        main.startColor = Humo_ColoridoPurpleColor;

        main = Bichitos.main;
        var auxColor = main.startColor;
        auxColor.colorMin = BichitosPurpleColor1;
        main.startColor = auxColor;

        main = Snow.main;
        main.startColor = SnowPurpleColor2;
    }
    public void SetColorBlue()
    {
        var main = Humo_Colorido.main;
        main.startColor = Humo_ColoridoBlueColor;

        main = Bichitos.main;
        var auxColor = main.startColor;
        auxColor.colorMin = BichitosBLueColor1;
        main.startColor = auxColor;

        main = Snow.main;
        main.startColor = SnowBLueColor2;
    }
    public void SetColorPink()
    {
        var main = Humo_Colorido.main;
        main.startColor = Humo_ColoridoPinkColor;

        main = Bichitos.main;
        var auxColor = main.startColor;
        auxColor.colorMin = BichitosPinkColor1;
        main.startColor = auxColor;

        main = Snow.main;
        main.startColor = SnowPinkColor2;
    }
   
}
